﻿using System;
using Gtk;

public partial class MainWindow: Gtk.Window
{
	int counter = 0;
	int oper = 0;
	float num1, num2;
	Operacoes calc = new Operacoes();
	float resultado;

	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	protected void OnBoffClicked (object sender, EventArgs e)
	{
		Application.Quit ();
	}

	protected void OnBdelClicked (object sender, EventArgs e)
	{
		entry.DeleteText (0, entry.Text.Length);
		num1 = 0;
		num2 = 0;
	}

	protected void OnBrClicked (object sender, EventArgs e)
	{
		entry.DeleteText (entry.Text.Length-1, entry.Text.Length);
	}

	protected void OnBdivClicked (object sender, EventArgs e)
	{
		if (oper == 0)
		{
			num1 = Convert.ToSingle(entry.Text);
			entry.DeleteText (0, entry.Text.Length);
			counter = 0;
			oper = 1;
		}
	}

	protected void OnB7Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "7");
	}

	protected void OnB8Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "8");
	}

	protected void OnB9Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "9");
	}

	protected void OnBmultClicked (object sender, EventArgs e)
	{
		if (oper == 0)
		{
			num1 = Convert.ToSingle(entry.Text);
			entry.DeleteText (0, entry.Text.Length);
			counter = 0;
			oper = 2;
		}
	}

	protected void OnB4Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "4");
	}

	protected void OnB5Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "5");
	}

	protected void OnB6Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "6");
	}

	protected void OnBminClicked (object sender, EventArgs e)
	{
		if (oper == 0)
		{
			num1 = Convert.ToSingle(entry.Text);
			entry.DeleteText (0, entry.Text.Length);
			counter = 0;
			oper = 3;
		}
	}

	protected void OnB1Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "1");
	}

	protected void OnB2Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "2");
	}

	protected void OnB3Clicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "3");
	}

	protected void OnBsumClicked (object sender, EventArgs e)
	{
		if (oper == 0)
		{
			num1 = Convert.ToSingle(entry.Text);
			entry.DeleteText (0, entry.Text.Length);
			counter = 0;
			oper = 4;
		}
	}

	protected void OnBClicked (object sender, EventArgs e)
	{
		string display = entry.Text.ToString();
		entry.DeleteText (0, entry.Text.Length);
		entry.InsertText (display + "0");
	}

	protected void OnBvirgClicked (object sender, EventArgs e)
	{
		if (counter == 0) {
			string display = entry.Text.ToString();
			entry.DeleteText (0, entry.Text.Length);
			entry.InsertText (display + ".");
			counter = 1;
		}
	}

	protected void OnBequalClicked (object sender, EventArgs e)
	{
		if (oper != 0) {
			num2 = Convert.ToSingle (entry.Text);
			entry.DeleteText (0, entry.Text.Length);
			switch (oper) {
			case 1:
				resultado = calc.Div (num1, num2);
				entry.InsertText (Convert.ToString (resultado));
				break;
			case 2:
				resultado = calc.Mult (num1, num2);
				entry.InsertText (Convert.ToString (resultado));
				break;
			case 3:
				resultado = calc.Sub (num1, num2);
				entry.InsertText (Convert.ToString (resultado));
				break;
			case 4:
				resultado = calc.Sum (num1, num2);
				entry.InsertText (Convert.ToString (resultado));
				break;
			}
			oper = 0;
		}
	}
}
